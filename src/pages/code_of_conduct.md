# Just Another Engineer (jae) Code of Conduct

## Introduction

Welcome to Just Another Engineer (jae)! We are committed to creating a respectful and inclusive community where everyone can contribute, collaborate, and engage in a positive manner. As a contributor, maintainer, or participant in this project, you are expected to follow this Code of Conduct to ensure a safe and welcoming environment for all.

## Our Values

We value the following principles and expect all community members to abide by them:

- **Respect**: Treat all individuals with respect and consideration, regardless of their background, identity, or differences.

- **Inclusivity**: Foster an inclusive environment that welcomes people of all backgrounds, experiences, and perspectives.

- **Kindness**: Be kind, compassionate, and understanding in your interactions with others.

- **Collaboration**: Encourage and support collaborative efforts, and be open to constructive feedback and criticism.

- **Patience**: Be patient and understanding with others, especially those who are new to the project or community.

## Unacceptable Behavior

The following behaviors are considered unacceptable within the jae community and will not be tolerated:

- Harassment, discrimination, or any form of intimidation or bullying.
- Offensive, hurtful, or derogatory comments, whether related to an individual's identity, background, or opinions.
- Personal attacks, including but not limited to public or private threats.
- Unwanted or inappropriate advances.
- Disruptive, spammy, or excessive self-promotion.
- Impersonation of others or misrepresentation of your identity.
- Any behavior that violates applicable laws or regulations.

## Reporting Violations

If you witness or experience behavior that violates this Code of Conduct, please report it immediately to [luiz@justanother.engineer]. All reports will be kept confidential. Our team will investigate and take appropriate action, which may include warnings, temporary or permanent bans, or other actions as necessary.

## Enforcement

Maintainers of the jae project are responsible for enforcing this Code of Conduct. They have the authority to address and resolve violations. However, they will also strive to ensure a fair and unbiased process.

## Consequences

Unacceptable behavior will not be tolerated within the jae community. Consequences for violating this Code of Conduct may include:

- A warning or request for a private apology.
- Temporary or permanent exclusion from the community, including revocation of project privileges.
- Removal of contributions or comments.
- Reporting to appropriate authorities in cases of illegal activities.

## Acknowledgment

By participating in the jae community, you agree to adhere to this Code of Conduct. We appreciate your efforts in making this community a welcoming and inclusive place for all.

This Code of Conduct is adapted from the [Contributor Covenant](https://www.contributor-covenant.org/version/2/0/code_of_conduct.html).

---

For questions, concerns, or to report violations, please contact [luiz@justanother.engineer].

