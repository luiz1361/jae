# Contributing

We welcome contributions from the community to help improve and grow this project. Whether you're updating content, reporting a bug, suggesting new features, or contributing code, your input is valuable to us.

Please take a moment to read this document to understand how you can contribute effectively.

## Table of Contents
1. [Code of Conduct](#code-of-conduct)
2. [How to Contribute](#how-to-contribute)
    - [Reporting Bugs](#reporting-bugs)
    - [Requesting Features](#requesting-features)
    - [Contributing Code](#contributing-code)
3. [Getting Started](#getting-started)
4. [Development Setup](#development-setup)
5. [Submitting a Pull Request](#submitting-a-pull-request)
6. [License](#license)

## Code of Conduct

Please note that this project has a [Code of Conduct](../code_of_conduct). By participating in this project, you are expected to uphold this code. Please report any unacceptable behavior to [luiz@justanother.engineer].

## How to Contribute

### Reporting Bugs

If you encounter a bug, please check the [Issue Tracker](https://gitlab.com/justanotherengineer/jae/issues) to see if it has already been reported. If not, please open a new issue and provide the following information:

- A clear and descriptive title.
- A detailed description of the issue, including steps to reproduce (if applicable).
- Information about your environment (e.g., operating system, browser, version).
- Any relevant screenshots or error messages.

### Requesting Features

We welcome feature requests! To request a new feature, please create an issue on the [Issue Tracker](https://gitlab.com/justanotherengineer/jae/issues) with the following:

- A clear and concise title.
- A detailed description of the feature you're proposing.

### Contributing Code

We appreciate contributions in the form of code changes, bug fixes, or new features. To contribute code:

1. Fork the repository.
2. Clone your fork to your local machine: `git clone git@gitlab.com:justanotherengineer/jae.git`
3. Create a new branch: `git checkout -b my-feature-branch`
4. Make your changes and test them thoroughly.
5. Commit your changes: `git commit -m "Add my new feature"`
6. Push your branch to your fork: `git push origin my-feature-branch`
7. Open a Merge Request on the [GitLab Repository](https://gitlab.com/justanotherengineer/jae) with a clear title and description.

## Getting Started

If you're new to contributing to open source projects, you can start by looking at the [good first issue](https://gitlab.com/justanotherengineer/jae/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=good+first+issue) label on our issue tracker. These are issues that are a good entry point for new contributors.

## Development Setup

If you want to set up a development environment, refer to the [Development](../development) for detailed instructions.

## Submitting a Pull Request

When submitting a pull request, make sure it meets the following criteria:

- Your code is well-documented.
- It follows the project's coding guidelines.
- All tests pass successfully.
- It addresses an existing issue or fulfills a documented feature request.

Please be patient during the review process. Reviewers will provide feedback, and you may need to make changes before your pull request is merged.

## License

By contributing to Just Another Engineer, you agree that your contributions will be licensed under the [MIT License](../license).

