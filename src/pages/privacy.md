# Privacy Policy

This privacy policy explains how we collect, use, and protect personal information that you provide to us through our website. By using our website, you agree to the terms of this privacy policy.

## Information We Collect

We may collect personal information from you when you:

- Register on our website
- Place an order
- Fill out a form
- Subscribe to our newsletter
- Use other features on our website

The types of personal information we may collect include your:

- Name
- Email address
- Mailing address
- Phone number
- Payment information

## How We Use Your Information

We use the personal information you provide to us for the following purposes:

- To process orders and provide customer service
- To personalize your experience on our website
- To send periodic emails about our products, services, and promotions
- To improve our website and offerings

We may also use your personal information to comply with applicable laws and regulations.

## Information Sharing

We do not sell, trade, or otherwise transfer your personal information to third parties without your consent, except as described below:

- We may share your personal information with third-party service providers who assist us in operating our website, conducting our business, or servicing you.
- We may release your personal information when we believe release is appropriate to comply with the law, enforce our website policies, or protect ours or others' rights, property, or safety.
- We may also share non-personally identifiable information with third-party advertisers, such as website usage data, to help them better understand our audience and improve their advertising efforts.

## Security

We take reasonable measures to protect your personal information from unauthorized access, use, or disclosure. We use encryption and other security measures to protect sensitive information transmitted online, and we restrict access to personal information to employees who need to know that information to perform their duties.

## Cookies

Our website may use cookies or similar technologies to enhance your user experience and collect website usage data. You can choose to disable cookies in your browser settings, but doing so may affect your ability to use certain features of our website.

## Children's Privacy

Our website is not intended for use by children under the age of 13. We do not knowingly collect personal information from children under the age of 13 without parental consent. If you believe we have collected personal information from a child under the age of 13 without parental consent, please contact us immediately, and we will take steps to delete the information as soon as possible.

## Changes to this Policy

We may update this privacy policy from time to time to reflect changes in our practices or legal obligations. We will post the updated policy on our website and indicate the date of the latest revision. Your continued use of our website after any changes to this policy indicates your acceptance of the updated policy.

## Contact Us

If you have any questions or concerns about our privacy policy or the handling of your personal information, please contact us using the information provided on our website.

