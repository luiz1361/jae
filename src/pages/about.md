# About Us

Welcome to our cheatsheet revolution! We're excited to introduce you to a cheatsheet solution that was born out of a need for simplicity, flexibility, and accessibility. Our website, created in 2019 by Luiz, was born from the frustration of struggling with various tools and their limitations.

## Our Technology Stack

We're proud to share the technology stack that powers our platform, ensuring that you have the best cheatsheet experience possible.

- **Platform**: Our website is powered by **Docusaurus**, a modern static website generator. Docusaurus provides a solid foundation for delivering content quickly and efficiently.

- **Fast and Scalable**: To ensure lightning-fast performance, we utilize **Cloudflare** as our Content Delivery Network (CDN). With Cloudflare, we cache content on edge locations worldwide, making your access to cheatsheets even faster.

- **Comprehensive Search**: We've partnered with **Algolia** to provide you with a robust and comprehensive search experience. Easily find the cheatsheets you need, even in a vast collection.

- **Versioning and Contribution**: Our platform is tightly integrated with **GitLab**, serving as the backbone for version control, hosting, collaboration, and continuous integration/continuous deployment (CI/CD). With GitLab, you can effortlessly track changes, collaborate with fellow users through issues and merge requests, and ensure you always have access to the most up-to-date versions of your cheatsheets via GitLab Pages.

- **Commenting and Collaboration**: Our vision extends beyond just cheatsheets. We aspire to build a vibrant community where users can collaborate seamlessly. In the near future, you'll have the ability to comment on shared cheatsheets and engage in discussions with others, fostering a sense of collaboration and knowledge sharing. We achieve this through **Disqus**, a powerful commenting platform that makes your cheatsheets a part of a larger conversation.

- **Monitoring**: Ensuring our platform's availability is crucial. We use **Pingdom** to monitor the availability of our website, ensuring that you can access your cheatsheets when you need them.

- **Communication**: We're committed to fostering a strong community of users. To facilitate communication, we've integrated a **Slack organization** that connects with GitLab, Pingdom, and other essential services. This Slack organization serves as a communication channel where you can interact with other users, ask questions, share feedback, and stay updated on platform updates and announcements.

## Our Story

The journey began with a simple question: How can we make cheatsheet creation and access easier and more efficient? It was recognized that there was a need for a better way to organize information without the hassles and limitations imposed by existing platforms.

### The Frustration

Before our website came into existence, various tools and platforms were tried, each with its own set of frustrations:

- **Evernote / OneNote / MediaWiki / DokuWiki**: Frustration of vendor lock-in and on-going subscription and/or hosting costs.

- **GitHub / Bitbucket**: The main issues were the search capabilities and limited mobile support.

- **OneDrive / Google Drive / DropBox**: Great for file storage, but not ideal for organizing and searching notes effectively.

- **Jekyll / Hugo / Wordpress / Drupal**: Finding an appropriate theme focused on documentation was not an easy task, hindering the ability to create an effective cheatsheet platform.

These attempts highlighted the need for a more tailored solution that met specific requirements.

### Our Vision

Our vision for this website was clear from the start:

- **Easy Access**: Access your cheatsheets seamlessly from any device, be it a mobile phone or a PC.

- **Speed**: A fast and responsive platform that doesn't keep you waiting.

- **Searchable**: Quickly find what you're looking for with robust search capabilities.

- **No Vendor Lock-In**: Freedom to use our service without worrying about being tied to a specific ecosystem.

- **Versioned**: Maintain a history of your cheatsheets to track changes and revisions.

- **Easy to Share**: Share your cheatsheets with colleagues, friends, or family members with ease.

- **Low Maintenance**: Minimal upkeep required, so you can focus on your cheatsheets, not server maintenance.

- **No Ongoing Cost**: Enjoy our service without the burden of recurring fees.

## Join Our Journey

We're proud of what we've accomplished so far, and we invite you to be a part of our cheatsheet revolution. Join us in simplifying and enhancing your cheatsheet experience.

Whether you're a student, a professional, or just someone who values organized information, we're here to make your life easier.

Thank you for choosing our platform. Your cheatsheets, your way.

> "Simplicity is the ultimate sophistication." – Leonardo da Vinci

