# tar

## Introduction

This cheat sheet provides a quick reference for common concepts and commands related to the `tar` command in Linux. `tar` is used for creating and manipulating tar archives, which are commonly used for bundling files and directories.

## `tar` Concepts

### Basic Usage

`tar` is primarily used for creating and extracting tar archives.

- Create a tar archive from files or directories:
  ```shell
  tar -cvf archive.tar file1 file2 directory/
  ```

- Extract files from a tar archive:
  ```shell
  tar -xvf archive.tar
  ```

### Compression

`tar` can compress archives using various compression algorithms.

- Create a compressed tar.gz archive:
  ```shell
  tar -czvf archive.tar.gz file1 file2 directory/
  ```

- Extract files from a compressed tar.gz archive:
  ```shell
  tar -xzvf archive.tar.gz
  ```

### Archive Listing

View the contents of a tar archive without extracting it.

- List the contents of a tar archive:
  ```shell
  tar -tvf archive.tar
  ```

- List the contents of a compressed tar.gz archive:
  ```shell
  tar -tzvf archive.tar.gz
  ```

### Extracting to a Specific Directory

Specify the destination directory when extracting files.

- Extract files to a specific directory:
  ```shell
  tar -xvf archive.tar -C /path/to/destination/
  ```

### Combining Files

You can use `tar` to combine multiple tar archives.

- Append files to an existing archive:
  ```shell
  tar -rvf existing-archive.tar newfile
  ```

### Compression Algorithms

`tar` supports various compression algorithms.

- Create a compressed tar.bz2 archive:
  ```shell
  tar -cjvf archive.tar.bz2 file1 file2 directory/
  ```

- Extract files from a compressed tar.bz2 archive:
  ```shell
  tar -xjvf archive.tar.bz2
  ```

### Working with Remote Servers

`tar` can be used to transfer files and directories over SSH.

- Create a tar archive and send it to a remote server:
  ```shell
  tar -czvf - directory/ | ssh user@remote "cat > /path/to/remote/archive.tar.gz"
  ```

- Extract a remote tar archive on the local machine:
  ```shell
  ssh user@remote "cat /path/to/remote/archive.tar.gz" | tar -xzvf -
  ```

## `tar` Command-Line

- Create a tar archive from files or directories:
  ```shell
  tar -cvf archive.tar file1 file2 directory/
  ```

- Extract files from a tar archive:
  ```shell
  tar -xvf archive.tar
  ```

- Create a compressed tar.gz archive:
  ```shell
  tar -czvf archive.tar.gz file1 file2 directory/
  ```

- Extract files from a compressed tar.gz archive:
  ```shell
  tar -xzvf archive.tar.gz
  ```

- List the contents of a tar archive:
  ```shell
  tar -tvf archive.tar
  ```

- Extract files to a specific directory:
  ```shell
  tar -xvf archive.tar -C /path/to/destination/
  ```

- Append files to an existing archive:
  ```shell
  tar -rvf existing-archive.tar newfile
  ```

- Create a compressed tar.bz2 archive:
  ```shell
  tar -cjvf archive.tar.bz2 file1 file2 directory/
  ```

- Extract files from a compressed tar.bz2 archive:
  ```shell
  tar -xjvf archive.tar.bz2
  ```

- Create a tar archive and send it to a remote server:
  ```shell
  tar -czvf - directory/ | ssh user@remote "cat > /path/to/remote/archive.tar.gz"
  ```

- Extract a remote tar archive on the local machine:
  ```shell
  ssh user@remote "cat /path/to/remote/archive.tar.gz" | tar -xzvf -
  ```

## Conclusion

This cheat sheet covers common concepts and commands for using the `tar` command in Linux. `tar` is a versatile tool for creating and manipulating tar archives, making it essential for bundling and compressing files and directories; refer to the [`tar` manual](https://man7.org/linux/man-pages/man1/tar.1.html) for more in-depth information and advanced usage.
