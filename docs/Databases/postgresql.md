# PostgreSQL

## Introduction

This cheat sheet provides a quick reference for some common PostgreSQL commands and concepts. PostgreSQL is a powerful open-source relational database management system (RDBMS) used for storing and managing data.

## Installation

To use PostgreSQL, you need to install it on your system. Installation methods vary depending on your operating system. Refer to the [official PostgreSQL documentation](https://www.postgresql.org/docs/current/installation.html) for installation instructions.

## PostgreSQL Concepts

### Databases

- Create a new database:
  ```sql
  CREATE DATABASE database_name;
  ```

- List all databases:
  ```sql
  \l
  ```

- Connect to a specific database:
  ```sql
  \c database_name
  ```

- Delete a database (Be cautious, this will remove all data):
  ```sql
  DROP DATABASE database_name;
  ```

### Tables

- Create a new table:
  ```sql
  CREATE TABLE table_name (
    column1 datatype,
    column2 datatype,
    ...
  );
  ```

- List all tables in the current database:
  ```sql
  \dt
  ```

- Describe the structure of a table:
  ```sql
  \d table_name
  ```

- Delete a table:
  ```sql
  DROP TABLE table_name;
  ```

### CRUD Operations

- Insert a new record into a table:
  ```sql
  INSERT INTO table_name (column1, column2, ...) VALUES (value1, value2, ...);
  ```

- Select data from a table:
  ```sql
  SELECT * FROM table_name WHERE condition;
  ```

- Update data in a table:
  ```sql
  UPDATE table_name SET column1 = value1, column2 = value2 WHERE condition;
  ```

- Delete data from a table:
  ```sql
  DELETE FROM table_name WHERE condition;
  ```

### Indexes

- Create an index on a table column:
  ```sql
  CREATE INDEX index_name ON table_name (column_name);
  ```

- List all indexes on a table:
  ```sql
  \di
  ```

### Joins

- Perform an INNER JOIN between two tables:
  ```sql
  SELECT * FROM table1 INNER JOIN table2 ON table1.column = table2.column;
  ```

### Functions

- Use built-in PostgreSQL functions:
  ```sql
  SELECT COUNT(*) FROM table_name;
  ```

### Users and Permissions

- Create a new PostgreSQL user:
  ```sql
  CREATE USER username WITH PASSWORD 'password';
  ```

- Grant privileges to a user:
  ```sql
  GRANT privilege_type ON TABLE table_name TO username;
  ```

- Revoke privileges from a user:
  ```sql
  REVOKE privilege_type ON TABLE table_name FROM username;
  ```

## PostgreSQL Command-Line Tool

- Log in to PostgreSQL using the command-line tool:
  ```shell
  psql -U username -d database_name -h hostname
  ```

- Execute SQL commands in batch mode:
  ```shell
  psql -U username -d database_name -a -f script.sql
  ```

## Conclusion

This cheat sheet covers some common PostgreSQL commands and concepts. PostgreSQL offers a wide range of features and functionality; refer to the [PostgreSQL documentation](https://www.postgresql.org/docs/current/index.html) for more in-depth information and advanced usage.
